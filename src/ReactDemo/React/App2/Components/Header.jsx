import React from "react";

import Title from "./Header/Title.jsx"

var Header = React.createClass({
    handleChange(e) {
        console.log("something");
        const title = e.target.value;
        this.props.changeTitle(title);
    },
    render() {
        //console.log(this.props);
        return (
            <div>
                <Title title={this.props.title}/>
                <input value={this.props.title} onChange={this.handleChange}/>
            </div>
        );
    }
});

module.exports = Header;