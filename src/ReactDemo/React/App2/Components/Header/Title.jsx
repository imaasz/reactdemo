import React from "react";

var Title = React.createClass({
    render() {
        return (
            <h1>{this.props.title}</h1>
        );
    }
});

module.exports = Title;