﻿import React from "react";

import Header from "./Components/Header.jsx";
import Footer from "./Components/Footer.jsx";

var App = React.createClass({
    getInitialState () {
        return {
            title: "Welcome"
        };
    },
    changeTitle(inputTitle) {
        this.setState({ title: inputTitle });
    },
    render() {
        return (
            <div>
                <p>{this.props.val1}</p>
                <p>{this.props.val2}</p>
                <Header changeTitle={this.changeTitle} title={this.state.title}/>
                <Footer />
            </div>
        )
    }
});

module.exports = App;