﻿import React from "react";
import ReactDOM from "react-dom";
import App from "./App/App.jsx";
//import Router from "react-router";
import {Router, Route, indexRouter, hashHistory} from "react-router";

require('expose-loader?ReactComponents!./App/index.js');

ReactDOM.render(
    <Router history={hashHistory}>
        <Route path="/" component={App}>
        </Route>
    </Router>,
    document.getElementById('app'));